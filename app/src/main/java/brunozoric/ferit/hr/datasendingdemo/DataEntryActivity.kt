package brunozoric.ferit.hr.datasendingdemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_data_entry.*

class DataEntryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_entry)
        setUpUi()
    }

    private fun setUpUi() = messageAction.setOnClickListener{ sendData() }

    private fun sendData() {
        val message = messageInput.text.toString()
        val size = messageFontSize.progress
        val displayIntent = Intent(this, DataDisplayActivity::class.java)
        displayIntent.putExtra(DataDisplayActivity.KEY_MESSAGE, message)
        displayIntent.putExtra(DataDisplayActivity.KEY_SIZE, size.toFloat())
        startActivity(displayIntent)
    }
}
