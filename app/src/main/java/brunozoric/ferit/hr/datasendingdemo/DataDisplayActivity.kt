package brunozoric.ferit.hr.datasendingdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_data_display.*

class DataDisplayActivity : AppCompatActivity() {

    companion object {
        const val KEY_MESSAGE: String = "message"
        const val KEY_SIZE: String = "size"
        const val DEFAULT_FONT_SIZE: Float = 12.0F
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_data_display)

        dataDisplay.text = intent?.getStringExtra(KEY_MESSAGE) ?: getString(R.string.unknownMessage)
        dataDisplay.textSize = intent?.getFloatExtra(KEY_SIZE, DEFAULT_FONT_SIZE) ?: DEFAULT_FONT_SIZE
    }
}
